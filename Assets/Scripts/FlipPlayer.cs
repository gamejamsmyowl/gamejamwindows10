﻿using UnityEngine;
using System.Collections;

public class FlipPlayer : MonoBehaviour {

        private float moveDirection;
        private bool facingRight = true;


        void FixedUpdate (){
            //Note: Physics should only be adjusted in FixedUpdate in order to allow for smooth physics

            //Grab our moving direction (-1 is left, 1 is right)
            //Go into Edit > Project Settings > Input Manager and change the sensitivity / gravity values
            // on the "Horizontal" button for loose (lower numbers) or tight (try 1000) controls.
            moveDirection = Input.GetAxis("Horizontal");

            //Move our player left/right based on which horizontal axis they are hitting (always returns -1, 0, or 1,
            // then multiply those values by mySpeed and Time.deltaTime. Set the Y velocity to the current players Y velocity,
            // meaning we don't really touch it at all (ie; jumping/falling gets left alone.
            //GetComponent<Rigidbody2D>().velocity = new Vector2(moveDirection * mySpeed * Time.deltaTime,GetComponent<Rigidbody2D>().velocity.y);

            
            //Flip our character around if they are not facing the correct direction
            if (moveDirection > 0 && !facingRight){
                FlipCharacter();
            }else if (moveDirection < 0 && facingRight){
                FlipCharacter();
            }
        }

        //Simple script to flip our character around. If we did scale x = -1, it would do weird things with the collider
        void FlipCharacter(){
            facingRight = !facingRight;

            if(facingRight){
                transform.rotation = Quaternion.Euler(0,0,0);
                Debug.Log("Direita");
            }
            else{
                transform.rotation = Quaternion.Euler(0,180,0);
                Debug.Log("Esquerda");
            }
        }



}
